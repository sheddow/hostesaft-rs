use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Seek;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;
use std::path::Path;
use std::path::PathBuf;

use clap::Parser;
use color_eyre::eyre;
use color_eyre::eyre::eyre;
use color_eyre::eyre::Context;
use indicatif::ProgressBar;
use url::Url;

mod compare;
use compare::ComparableResponse;

#[derive(Debug, PartialEq, Eq)]
enum Severity {
    None,
    Low,
    Medium,
    High,
}

struct ScanResult {
    vhost: String,
    severity: Severity,
    response: ComparableResponse,
}

impl ScanResult {
    fn write_to_file(&self, base_dir: impl AsRef<Path>) -> eyre::Result<()> {
        let out_dir = base_dir.as_ref().join(format!("{:?}", self.severity));
        std::fs::create_dir_all(&out_dir)?;
        let data = format!(
            "Status: {}\nBody:\n{}",
            self.response.status_code, &self.response.body
        );
        let path = out_dir.join(&self.vhost);
        std::fs::write(path, data).map_err(|e| e.into())
    }

    fn new(vhost: String, severity: Severity, response: ComparableResponse) -> Self {
        Self {
            vhost,
            severity,
            response,
        }
    }
}

struct Target {
    addr: SocketAddr,
    wildcard_resp: ComparableResponse,
    https: bool,
}

impl Target {
    fn new(addr: SocketAddr, https: bool, root_domain: &str) -> Self {
        let random_vhost = format!("doesntexistqwertyuiop.{root_domain}");
        let wildcard_resp = ComparableResponse::from_vhost(&random_vhost, addr, https);

        Target {
            addr,
            wildcard_resp,
            https,
        }
    }

    fn check_vhost(&self, vhost: &str) -> ScanResult {
        let resp = ComparableResponse::from_vhost(vhost, self.addr, self.https);

        if resp.status_code == 0 {
            return ScanResult::new(vhost.to_owned(), Severity::None, resp);
        }

        let direct_resp = ComparableResponse::from_vhost_direct(vhost, self.https);

        let matches_wildcard = resp.likely_eq(&self.wildcard_resp);
        let matches_direct = resp.likely_eq(&direct_resp);

        let sev = if matches_wildcard || matches_direct {
            Severity::None
        } else {
            match (resp.status_code, direct_resp.status_code) {
                (200, 200) if resp.compare(&direct_resp) > 0.95 => Severity::Medium,
                (200, _) => Severity::High,
                (300..=399, _) => Severity::Medium,
                _ => Severity::Low,
            }
        };

        ScanResult::new(vhost.to_owned(), sev, resp)
    }
}

#[derive(Parser)]
struct Cli {
    target: String,
    hosts: PathBuf,
    #[arg(short, long, value_name = "FILE")]
    out: Option<PathBuf>,
    #[arg(short, long)]
    domain: Option<String>,
}

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    let cli = Cli::parse();

    let url = Url::parse(&cli.target).wrap_err("Failed to parse URL")?;
    let file = File::open(cli.hosts).wrap_err("Failed to read host file")?;
    let out = cli.out.unwrap_or("out".into());

    let domain = url
        .host_str()
        .ok_or_else(|| eyre!("missing hostname in {url}"))?;
    let port = url
        .port_or_known_default()
        .ok_or_else(|| eyre!("missing port or unknown protocol: {url}"))?;

    let d = format!("{domain}:{port}");
    let target_addr = d
        .to_socket_addrs()
        .wrap_err_with(|| format!("Failed to look up {url}"))?
        .next()
        .unwrap();

    let https = match url.scheme() {
        "http" => false,
        "https" => true,
        _ => panic!("Invalid scheme"),
    };

    let root_domain = cli.domain.unwrap_or_else(|| domain.to_owned());
    let target = Target::new(target_addr, https, &root_domain);

    let num_lines = BufReader::new(file.try_clone()?).lines().map(|_| 1).sum();
    let mut reader = BufReader::new(file);
    reader.rewind()?;

    let bar = ProgressBar::new(num_lines);
    println!("Analyzing {num_lines} hosts");

    for line in reader.lines() {
        let host = line?;
        let res = target.check_vhost(&host);
        if res.severity != Severity::None {
            bar.println(format!("{} (Severity {:?})", host, res.severity));
            res.write_to_file(&out)?;
        }
        bar.inc(1);
    }

    bar.finish();

    Ok(())
}
