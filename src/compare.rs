use std::io::Read;
use std::net::SocketAddr;
use std::time::Duration;

use lol_html::{doc_text, element, HtmlRewriter, Settings};
use native_tls::TlsConnector;
use similar::TextDiff;
use std::sync::Arc;
use ureq::AgentBuilder;

const CONNECT_TIMEOUT: Duration = Duration::from_secs(5);

fn create_agent(target: Option<SocketAddr>) -> ureq::Agent {
    let tls_con = TlsConnector::builder()
        .danger_accept_invalid_certs(true)
        .danger_accept_invalid_hostnames(true)
        .build()
        .unwrap();

    let builder = AgentBuilder::new()
        .tls_connector(Arc::new(tls_con))
        .timeout_connect(CONNECT_TIMEOUT)
        .redirects(0);

    if let Some(target) = target {
        builder
            .resolver(move |_addr: &str| Ok(vec![target]))
            .build()
    } else {
        builder.build()
    }
}

pub struct ComparableResponse {
    pub vhost: String,
    pub body: String,
    pub status_code: u16,
    pub html: bool,
}

impl ComparableResponse {
    pub fn from_vhost(vhost: &str, target: SocketAddr, https: bool) -> Self {
        let agent = create_agent(Some(target));

        let url = format!("http{}://{vhost}", if https { "s" } else { "" });
        let res = agent
            .get(&url)
            .set("X-Forwarded-Host", vhost)
            .set("Forwarded", &format!("host={vhost}"))
            .call();

        Self::from_ureq_result(res, vhost)
    }

    pub fn from_vhost_direct(vhost: &str, https: bool) -> Self {
        let agent = create_agent(None);

        let url = format!("http{}://{vhost}", if https { "s" } else { "" });
        let res = agent.get(&url).call();

        Self::from_ureq_result(res, vhost)
    }

    fn from_ureq_result(res: Result<ureq::Response, ureq::Error>, vhost: &str) -> Self {
        match res {
            Ok(resp) | Err(ureq::Error::Status(_, resp)) => {
                let status = resp.status();
                if status > 300 && status < 399 {
                    let loc = resp
                        .header("Location")
                        .unwrap_or("")
                        .split('?')
                        .next()
                        .unwrap_or("");

                    Self::new(vhost, status, loc.to_owned(), false)
                } else {
                    let mut bytes = Vec::new();
                    let html = resp.header("Content-type").map_or(false, |x| {
                        x.contains("text/html") || x.contains("application/xml")
                    });
                    resp.into_reader()
                        .take(10_000)
                        .read_to_end(&mut bytes)
                        .unwrap();
                    Self::new(
                        vhost,
                        status,
                        String::from_utf8_lossy(&bytes).to_string(),
                        html,
                    )
                }
            }
            Err(ureq::Error::Transport(_)) => Self::new(vhost, 0, "".into(), false),
        }
    }

    fn new(vhost: &str, status_code: u16, body: String, html: bool) -> Self {
        Self {
            vhost: vhost.to_owned(),
            body,
            status_code,
            html,
        }
    }

    pub fn compare(&self, other: &ComparableResponse) -> f32 {
        let a = self.body.replace(&self.vhost, "");
        let b = other.body.replace(&other.vhost, "");
        let diff = TextDiff::from_chars(&a, &b);
        diff.ratio()
    }

    pub fn likely_eq(&self, other: &ComparableResponse) -> bool {
        let similarity = self.compare(other);
        self.status_code == other.status_code
            && (similarity >= 0.98
                || self.body.len() == other.body.len() && similarity >= 0.9
                || self.structurally_eq(other))
    }

    pub fn structurally_eq(&self, other: &ComparableResponse) -> bool {
        self.html && other.html && naked_html(&self.body) == naked_html(&other.body)
    }
}

fn naked_html(s: &str) -> String {
    let mut output = vec![];
    {
        let mut rewriter = HtmlRewriter::new(
            Settings {
                element_content_handlers: vec![element!("*", |el| {
                    let attr_names: Vec<_> = el.attributes().iter().map(|a| a.name()).collect();
                    for attr_name in attr_names {
                        el.set_attribute(&attr_name, "").unwrap();
                    }
                    Ok(())
                })],
                document_content_handlers: vec![doc_text!(|t| {
                    t.remove();
                    Ok(())
                })],
                ..Settings::default()
            },
            |c: &[u8]| output.extend_from_slice(c),
        );

        rewriter.write(s.as_bytes()).unwrap();
        rewriter.end().unwrap();
    }

    String::from_utf8_lossy(&output).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_naked_html() {
        let input = "<div id=asdf>hello, <span class=\"wow\">world!</span></div>";
        let result = naked_html(input);
        assert_eq!(result, "<div id=\"\"><span class=\"\"></span></div>");
    }

    #[test]
    fn test_naked_html_using_xml() {
        let input = r#"<Error>
<Code>AccessDenied</Code>
<Message>Access Denied</Message>
<RequestId>J70RANYTSTDY5S0Z</RequestId>
<HostId>
91cIG0YHzuvXCgyOY+semrZF2ag4IaZF02C6zEWy+QGuasYW5QTtSN6k=
</HostId>
</Error>"#;
        let result = naked_html(input);
        assert_eq!(result, "<Error><Code></Code><Message></Message><RequestId></RequestId><HostId></HostId></Error>");
    }
}
